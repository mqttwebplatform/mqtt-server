package com.mqtt.core.client;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class Publisher {

	private String brokerServer;

	public Publisher(String brokerServer) {
		super();
		this.brokerServer = brokerServer;
	}

	public void send(String title, String content) throws MqttException {

		MqttClient client = new MqttClient(brokerServer, MqttClient.generateClientId());
		client.connect();
		MqttMessage message = new MqttMessage();
		message.setPayload(content.getBytes());
		client.publish(title, message);

		client.disconnect();

	}

	public static void main(String[] args) throws MqttException {

		String messageString = "Hello World from Java!";

		if (args.length == 2) {
			messageString = args[1];
		}

		System.out.println("== START PUBLISHER ==");

		new Publisher("tcp://localhost:1883").send("HD/temp", messageString);

	}

}
