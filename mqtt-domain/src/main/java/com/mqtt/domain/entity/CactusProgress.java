/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mqtt.domain.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SimpleBoy
 */
@Entity
@Table(name = "cactus_progress", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CactusProgress.findAll", query = "SELECT c FROM CactusProgress c"),
    @NamedQuery(name = "CactusProgress.findById", query = "SELECT c FROM CactusProgress c WHERE c.id = :id"),
    @NamedQuery(name = "CactusProgress.findByCode", query = "SELECT c FROM CactusProgress c WHERE c.code = :code"),
    @NamedQuery(name = "CactusProgress.findByCactusWidth", query = "SELECT c FROM CactusProgress c WHERE c.cactusWidth = :cactusWidth"),
    @NamedQuery(name = "CactusProgress.findByCactusHeight", query = "SELECT c FROM CactusProgress c WHERE c.cactusHeight = :cactusHeight"),
    @NamedQuery(name = "CactusProgress.findByDescription", query = "SELECT c FROM CactusProgress c WHERE c.description = :description"),
    @NamedQuery(name = "CactusProgress.findByCreateon", query = "SELECT c FROM CactusProgress c WHERE c.createon = :createon"),
    @NamedQuery(name = "CactusProgress.findByStatus", query = "SELECT c FROM CactusProgress c WHERE c.status = :status")})
public class CactusProgress implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "code", nullable = false, length = 2000000000)
    private String code;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cactus_width", precision = 2000000000, scale = 10)
    private Double cactusWidth;
    @Column(name = "cactus_height", precision = 2000000000, scale = 10)
    private Double cactusHeight;
    @Column(name = "description", length = 2000000000)
    private String description;
    @Basic(optional = false)
    @Column(name = "createon", nullable = false)
    private double createon;
    @Basic(optional = false)
    @Column(name = "status", nullable = false)
    private int status;
    @JoinColumn(name = "createby", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createby;
    @JoinColumn(name = "cactus_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Cactus cactusId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cactusProgressId", fetch = FetchType.LAZY)
    private List<ProgressPicture> progressPictureList;

    public CactusProgress() {
    }

    public CactusProgress(Integer id) {
        this.id = id;
    }

    public CactusProgress(Integer id, String code, double createon, int status) {
        this.id = id;
        this.code = code;
        this.createon = createon;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getCactusWidth() {
        return cactusWidth;
    }

    public void setCactusWidth(Double cactusWidth) {
        this.cactusWidth = cactusWidth;
    }

    public Double getCactusHeight() {
        return cactusHeight;
    }

    public void setCactusHeight(Double cactusHeight) {
        this.cactusHeight = cactusHeight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getCreateon() {
        return createon;
    }

    public void setCreateon(double createon) {
        this.createon = createon;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public User getCreateby() {
        return createby;
    }

    public void setCreateby(User createby) {
        this.createby = createby;
    }

    public Cactus getCactusId() {
        return cactusId;
    }

    public void setCactusId(Cactus cactusId) {
        this.cactusId = cactusId;
    }

    @XmlTransient
    public List<ProgressPicture> getProgressPictureList() {
        return progressPictureList;
    }

    public void setProgressPictureList(List<ProgressPicture> progressPictureList) {
        this.progressPictureList = progressPictureList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CactusProgress)) {
            return false;
        }
        CactusProgress other = (CactusProgress) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CactusProgress[ id=" + id + " ]";
    }
    
}
