package com.mqtt.domain.entity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SimpleBoy
 */
@Entity
@Table(name = "attach_file", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AttachFile.findAll", query = "SELECT a FROM AttachFile a"),
    @NamedQuery(name = "AttachFile.findById", query = "SELECT a FROM AttachFile a WHERE a.id = :id"),
    @NamedQuery(name = "AttachFile.findByCode", query = "SELECT a FROM AttachFile a WHERE a.code = :code"),
    @NamedQuery(name = "AttachFile.findByFilename", query = "SELECT a FROM AttachFile a WHERE a.filename = :filename"),
    @NamedQuery(name = "AttachFile.findByType", query = "SELECT a FROM AttachFile a WHERE a.type = :type"),
    @NamedQuery(name = "AttachFile.findBySize", query = "SELECT a FROM AttachFile a WHERE a.size = :size"),
    @NamedQuery(name = "AttachFile.findByPath", query = "SELECT a FROM AttachFile a WHERE a.path = :path"),
    @NamedQuery(name = "AttachFile.findByCreateon", query = "SELECT a FROM AttachFile a WHERE a.createon = :createon"),
    @NamedQuery(name = "AttachFile.findByStatus", query = "SELECT a FROM AttachFile a WHERE a.status = :status")})
public class AttachFile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "code", nullable = false, length = 2000000000)
    private String code;
    @Basic(optional = false)
    @Column(name = "filename", nullable = false, length = 2000000000)
    private String filename;
    @Basic(optional = false)
    @Column(name = "type", nullable = false, length = 2000000000)
    private String type;
    @Basic(optional = false)
    @Column(name = "size", nullable = false)
    private double size;
    @Basic(optional = false)
    @Column(name = "path", nullable = false, length = 2000000000)
    private String path;
    @Basic(optional = false)
    @Column(name = "createon", nullable = false)
    private double createon;
    @Basic(optional = false)
    @Column(name = "status", nullable = false)
    private int status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "attachFileId", fetch = FetchType.LAZY)
    private List<ProgressPicture> progressPictureList;
    @JoinColumn(name = "createby", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createby;
    @OneToMany(mappedBy = "icon", fetch = FetchType.LAZY)
    private List<User> userList;

    public AttachFile() {
    }

    public AttachFile(Integer id) {
        this.id = id;
    }

    public AttachFile(Integer id, String code, String filename, String type, double size, String path, double createon, int status) {
        this.id = id;
        this.code = code;
        this.filename = filename;
        this.type = type;
        this.size = size;
        this.path = path;
        this.createon = createon;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public double getCreateon() {
        return createon;
    }

    public void setCreateon(double createon) {
        this.createon = createon;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @XmlTransient
    public List<ProgressPicture> getProgressPictureList() {
        return progressPictureList;
    }

    public void setProgressPictureList(List<ProgressPicture> progressPictureList) {
        this.progressPictureList = progressPictureList;
    }

    public User getCreateby() {
        return createby;
    }

    public void setCreateby(User createby) {
        this.createby = createby;
    }

    @XmlTransient
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AttachFile)) {
            return false;
        }
        AttachFile other = (AttachFile) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.AttachFile[ id=" + id + " ]";
    }
    
}
